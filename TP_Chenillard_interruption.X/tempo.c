#include <pic18f46k22.h>

void delai(){
    T0CON = 0x81; // Initialisation du prescaler
    while(1){
        if(INTCONbits.TMR0IF == 1) { // Lorsque le flag de flood se lève, il est directement redescendu
            INTCONbits.TMR0IF = 0;
            return;
        }
    }
}
