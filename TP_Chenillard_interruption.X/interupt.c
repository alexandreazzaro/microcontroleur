#include <pic18f46k22.h>
#include <xc.h>
#include "general.h"

void __interrupt() arret(void) {
    if (INTCONbits.INT0IF) {
        INTCONbits.INT0IF = 0; /* Clear INT0IF flag*/
        if (TMR0ON == 0)
            TMR0ON = 1;
        else
            TMR0ON = 0;
        return;
    }
    return;
}
